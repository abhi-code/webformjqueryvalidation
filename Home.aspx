﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="jQueryValidation.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>jQuery Validation</title>
    <link href="~/Content/bootstrap.min.css" rel="stylesheet" />
    <link href="~/Content/bootstrap.css" rel="stylesheet" />
    <link href="~/Content/fontawesome-all.min.css" rel="stylesheet" />
    <script src="/Scripts/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.validate.min.js" type="text/javascript"></script>
    <script src="https://kit.fontawesome.com/da45f1b7be.js"></script>
    <script src="/Scripts/fontawesome/all.min.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $.validator.addMethod("emailMatch", function (value, element) {
                return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
            }, "Invalid Email."); 
            $.validator.addMethod("CheckDropDownList", function(value, element, param) {
                if (value === '0')
                    return false;
                else
                    return true;
                },"Please select a role");
            $("#form1").validate({
                rules: {
                    <%=txtName.UniqueID %>: { required: true },
                    <%=txtEmail.UniqueID %>: { required: true, emailMatch: true },
                    <%=txtAge.UniqueID %>: { required: true, range: [18, 30] },
                    <%=txtPwd.UniqueID %>: { required: true },
                    <%=txtRetypePwd.UniqueID %>: { required: true, equalTo: "#<%=txtPwd.UniqueID %>" },
                    <%=txtUName.UniqueID %>: { required: true },
                    <%=ddlRole.UniqueID %>: { CheckDropDownList: true },
                    <%=CheckBox1.UniqueID %>: { required: true },
                },
                messages: {
                    <%=txtName.UniqueID %>: { required: "Name is required" },
                    <%=txtEmail.UniqueID %>: { required: "Email is required" },
                    <%=txtAge.UniqueID %>: { required: "Age is required", range: "Age should be between {0} and {1}" },
                    <%=txtPwd.UniqueID %>: { required: "Password is required" },
                    <%=txtRetypePwd.UniqueID %>: { required: "Password is required", equalTo: "Enter same Password" },
                    <%=txtUName.UniqueID %>: { required: "Username is required" },
                    <%=CheckBox1.UniqueID %>: { required: "Please accept the Terms & Conditions" },
                },
                errorPlacement: function(error, element) {
                    if (element.is(":checkbox")) {
                        error.appendTo(element.parents('.chckbx'));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });
    </script>
    
    <style type ="text/css" >  
        label.error {             
            color: red;   
            display: inline-flex ;                 
        }  
    </style>  
</head>
<body>
    <div class="container">
        <div class="row justify-content-center mx-auto mt-lg-5 mb-lg-5">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Register</div>
                    <div class="card-body">
                        <form id="form1" class="form-horizontal" runat="server">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Your Name</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="col-sm-1 input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <asp:TextBox ID="txtName" CssClass="form-control" runat="server" ToolTip="Enter your name"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Your Email</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="col-sm-1 input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                        <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" ></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Your Age</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="col-sm-1 input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                        <asp:TextBox ID="txtAge" CssClass="form-control"  runat="server" ToolTip="Enter your Age" TextMode="Number"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="col-sm-1 input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                                        <asp:TextBox ID="txtUName" CssClass="form-control"  runat="server" ToolTip="Enter your Username"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="col-sm-1 input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                        <asp:TextBox ID="txtPwd" CssClass="form-control"  runat="server" ToolTip="Enter your Password" TextMode="Password"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Confirm Password</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="col-sm-1 input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                        <asp:TextBox ID="txtRetypePwd" CssClass="form-control"  runat="server" ToolTip="Confirm you Password" TextMode="Password"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Role:</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="col-sm-1 input-group-addon"><i class="fas fa-user-tag fa-lg" aria-hidden="true"></i></span>
                                        <asp:DropDownList ID="ddlRole" runat="server">
                                            <asp:ListItem Selected="True" Value="0">Selected</asp:ListItem>
                                            <asp:ListItem Value="1">User</asp:ListItem>
                                            <asp:ListItem Value="2">Admin</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="chckbx form-group">
                                <asp:CheckBox ID="CheckBox1" runat="server" Text="Accept Terms &amp; Conditions." />
                            </div>
                            <div class="form-group ">
                                <asp:Button ID="BtnSignUp" CssClass="btn btn-primary btn-lg btn-block login-button" Text="Register" runat="server"/>
                            </div>
                            <div class="login-register">
                                <asp:LinkButton Text="Login" ID="BtnLogin" runat="server"></asp:LinkButton>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
